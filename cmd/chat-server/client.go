// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	pb "gitlab.com/manhrev/chat-app1/api/chatpb"
	"gitlab.com/manhrev/chat-app1/auth"
	"google.golang.org/protobuf/proto"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	idx      int
	username string
	hub      *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *pb.WSMessage
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		//log.Println(message)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		//convert message ([]byte) to pb.WSMessage
		//c.hub.broadcast <- message
		var convertedMessage pb.WSMessage

		//log.Println(convertedMessage.GetCreateChannelRequest().GetUserName())
		err = proto.Unmarshal(message, &convertedMessage)
		if err != nil {
			log.Print("Convert to WSMessage failed")
		}
		convertedMessage.UserName = c.username
		convertedMessage.ClientId = int32(c.idx)
		//send convertedMessage to hub
		c.hub.broadcast <- &convertedMessage
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}
			convertedMessage, err := proto.Marshal(message)
			if err != nil {
				log.Println("Convert WSmessage failed")
			}

			w.Write(convertedMessage)
			log.Println("send...............................", c.username)
			// fmt.Printf("%T", &convertedMessage)
			// log.Println("send a message to user")
			//Add queued chat messages to the current websocket message.
			// n := len(c.send)
			// for i := 0; i < n; i++ {
			// 	w.Write(newline)
			// 	w.Write(<-c.send)
			// }

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	//authenticate
	token := r.URL.Query().Get("token")
	username, err := auth.VerifyToken(token)
	if err != nil {
		log.Println("Authentication failed", err)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	//for demo
	// log.Println("wait for username")
	// _, usermess, _ := conn.ReadMessage()
	// username := string(bytes.TrimSpace(bytes.Replace(usermess, newline, space, -1)))

	// //log.Println("upgraded websocket connection")
	// u, err := db.QueryUser(username, context.Background())
	// //user not in database
	// if err != nil {
	// 	u, err = db.CreateUser("USERNAME", username, "pass", context.Background())
	// 	//cannot create user
	// 	if err != nil {
	// 		log.Println("cannot create user")
	// 		return
	// 	}
	// } else {
	// 	username = u.Username
	// }

	client := &Client{username: username, hub: hub, conn: conn, send: make(chan *pb.WSMessage, 5), idx: -1}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()

	log.Println("user", username, "connected success")

}
