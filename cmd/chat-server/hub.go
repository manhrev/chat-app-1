// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"log"
	"strconv"

	pb "gitlab.com/manhrev/chat-app1/api/chatpb"
	db "gitlab.com/manhrev/chat-app1/database"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type ChatRoom struct {
	owner_username string
	room_name      string
	users          map[string]bool
}

func newChatRoom(owner_username string, room_users []string, room_name string) *ChatRoom {
	users := make(map[string]bool)
	for _, username := range room_users {
		users[username] = true
	}
	users[owner_username] = true
	return &ChatRoom{
		owner_username: owner_username,
		users:          users,
		room_name:      room_name,
	}
}

type Hub struct {
	// Max idx for same client username, must be started from 1
	clientsMaxIdx map[string]int

	// Registered clients.
	clients map[string](map[int]*Client)

	// Inbound messages from the clients.
	broadcast chan *pb.WSMessage

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	chatRooms map[string]*ChatRoom
}

func (h *Hub) addNewRoom(room_id int, owner_username string, room_users []string, room_name string) {
	roomidstr := strconv.Itoa(room_id)
	//log.Println("room users", room_users)
	if h.chatRooms[roomidstr] != nil {
		return
	}
	h.chatRooms[roomidstr] = newChatRoom(owner_username, room_users, room_name)
}

func newHub() *Hub {
	hub := &Hub{
		broadcast:     make(chan *pb.WSMessage),
		register:      make(chan *Client),
		unregister:    make(chan *Client),
		clients:       make(map[string](map[int]*Client)),
		clientsMaxIdx: make(map[string]int),
		chatRooms:     make(map[string]*ChatRoom),
	}
	//test
	//hub.chatRooms["room1"] = newChatRoom("manh", []string{"manh", "tuan", "tai"}, "sample room")
	return hub
}

//update a room when db change
func (h *Hub) update_room(room_id int) {
	room, users, err := db.GetRoomForUpdate(room_id, context.Background())
	if err != nil {
		//log.Print("beasfsadgassadfaraes")
		delete(h.chatRooms, strconv.Itoa(room_id))
	}
	ru := h.chatRooms[strconv.Itoa(room_id)]
	if ru != nil {
		var usersn = make(map[string]bool)
		for _, u := range users {
			usersn[u.Username] = true
		}
		ru.room_name = room.RoomName
		ru.users = usersn
	}
}

func (h *Hub) init_rooms_for(username string) {
	rooms, err := db.GetRoomsForUser(username, context.Background())
	if err != nil {
		return
	}
	for _, room := range rooms {
		//if chatroom already exists in hub
		if h.chatRooms[strconv.Itoa(room.ID)] != nil {
			continue
		}
		var room_users []string
		db_room_users := room.QueryUsers().AllX(context.Background())
		for _, room_user := range db_room_users {
			room_users = append(room_users, room_user.Username)
		}
		owner, _ := room.QueryOwner().Only(context.Background())

		h.addNewRoom(room.ID, owner.Username, room_users, room.RoomName)
		//log.Println("Init room:", room.ID)
	}
}

func (h *Hub) clear_rooms_for(username string) {
	for roomstr, room := range h.chatRooms {
		hubRoomEmpty := true
		for user := range room.users {
			if h.clients[user] != nil {
				hubRoomEmpty = false
			}
		}
		if hubRoomEmpty {
			if h.chatRooms[roomstr] != nil {
				//h.chatRooms[roomstr] = nil
				delete(h.chatRooms, roomstr)
			}

			//log.Println("Removed room:", roomstr)
		}
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			username := client.username

			// if username already online
			if h.clientsMaxIdx[client.username] == 0 {
				h.clients[username] = make(map[int]*Client)
				h.clientsMaxIdx[username] = 1
				h.clients[client.username][h.clientsMaxIdx[username]] = client
				client.idx = h.clientsMaxIdx[username]
				h.clientsMaxIdx[username]++
			} else {
				h.clients[username][h.clientsMaxIdx[username]] = client
				client.idx = h.clientsMaxIdx[username]
				h.clientsMaxIdx[username]++
			}
			h.init_rooms_for(client.username)
			// fmt.Print("users active:")
			// for u := range h.clients {
			// 	fmt.Print(u, " ")
			// }

		case client := <-h.unregister:
			username := client.username
			idx := client.idx

			if _, ok := h.clients[username]; ok {
				if _, ok := h.clients[username][idx]; ok {
					delete(h.clients[username], idx)
					close(client.send)
				}

				//check if that user not online
				if len(h.clients[username]) == 0 {
					delete(h.clients, username)
					delete(h.clientsMaxIdx, username)
					h.clear_rooms_for(client.username)
				}

			}

			//also check and close rooms here!

		case message := <-h.broadcast:
			if message.GetShowActiveUsersRequest() != nil {
				log.Println("show users request")
				m := message.GetShowActiveUsersRequest()
				h.showActiveUsersHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetShowMyRoomsRequest() != nil {
				log.Println("show rooms request")
				m := message.GetShowMyRoomsRequest()
				h.showMyRoomsHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetChatRequest() != nil {
				log.Println("chat request")
				m := message.GetChatRequest()
				h.chatMessageHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetCreateChannelRequest() != nil {
				log.Println("create channel request")
				m := message.GetCreateChannelRequest()
				h.createChannelHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetStateRequest() != nil {
				log.Println("get state request")
				m := message.GetStateRequest()
				h.getStateHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetJoinRequest() != nil {
				log.Println("join request")
				m := message.GetJoinRequest()
				h.joinRequestHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetLeaveRequest() != nil {
				log.Println("leave request")
				m := message.GetLeaveRequest()
				h.leaveRequestHandle(m, message.GetUserName(), int(message.GetClientId()))
			}
			if message.GetRemoveChannelRequest() != nil {
				log.Print("remove channel request")
				m := message.GetRemoveChannelRequest()
				h.removeChannelRequestHandle(m, message.GetUserName(), int(message.GetClientId()))
			}

		}
	}
}

func (h *Hub) showActiveUsersHandle(m *pb.ShowActiveUsersRequest, username string, clientIdx int) {
	var all_active_users []string
	for user := range h.clients {
		all_active_users = append(all_active_users, user)
	}
	//log.Println(all_active_users)

	client := h.clients[username]
	if client != nil {
		for _, clienti := range client {
			if clienti.idx == clientIdx {
				clienti.send <- &pb.WSMessage{
					Payload: &pb.WSMessage_ShowActiveUsersReply{
						ShowActiveUsersReply: &pb.ShowActiveUsersReply{
							AllUserNames: all_active_users,
						},
					},
				}
			}
		}
	}
}

func (h *Hub) showMyRoomsHandle(m *pb.ShowMyRoomsRequest, username string, clientIdx int) {
	h.init_rooms_for(username)
	var availableRooms []*pb.Room
	for room := range h.chatRooms {
		//log.Println(room)
		for user := range h.chatRooms[room].users {

			if user == username {
				availableRooms = append(availableRooms, &pb.Room{
					RoomId:   room,
					RoomName: h.chatRooms[room].room_name,
				})
			}
		}
	}
	client := h.clients[username]
	if client != nil {
		for _, clienti := range client {
			if clienti.idx == clientIdx {

				clienti.send <- &pb.WSMessage{
					Payload: &pb.WSMessage_ShowMyRoomsReply{
						ShowMyRoomsReply: &pb.ShowMyRoomsReply{
							AllRoomIds: availableRooms,
						},
					},
				}
			}
		}
		//log.Println("sent rooms")
	}
}

func (h *Hub) createChannelHandle(m *pb.CreateChannelRequest, username string, clientIdx int) {
	owner := username
	users := m.GetUsernameToJoin()

	//log.Println("room users:", users)
	room_name := m.GetRoomName()
	//log.Println("roomname", room_name)
	//create room in database
	_, err := db.CreateRoom(owner, room_name, append(users, username), context.Background())
	if err != nil {
		//create room error
		log.Println(err)
		return
	} else {
		//h.addNewRoom(owner, users, room.RoomName)
		//h.init_rooms_for(username)
	}

	//log.Println(owner, users, h.chatRooms["0"])

	//createchannelreply

}

func (h *Hub) chatMessageHandle(m *pb.ChatRequest, username string, clientIdx int) {
	//set message sent by this user
	m.GetMessage().UserSent = username

	sent_to_roomid := m.GetMessage().GetRoomId()
	user_sent := username

	//save to db
	sent_to_room, err := strconv.Atoi(sent_to_roomid)
	if err != nil {
		return
	}
	_, err = db.SaveMessage(username, sent_to_room, m.GetMessage().GetMessage(), context.Background())
	if err != nil {
		log.Println("Cannot save message to database, cancel reply")
		return
	}
	room := h.chatRooms[sent_to_roomid]
	if room != nil {
		user_sent_in_room := room.users[user_sent]
		if user_sent_in_room == true {
			for username, _ := range room.users {
				//create chatmessage and send to all room ussers (type WSMessage)
				client := h.clients[username]
				if client != nil {
					for _, clienti := range client {
						clienti.send <- &pb.WSMessage{
							Payload: &pb.WSMessage_ChatReply{
								ChatReply: &pb.ChatReply{
									Message: m.Message,
								},
							},
						}
					}
					//log.Println("send", m.Message.UserSent)
				}

			}
			//create chatreply and send to usersent
			//...

		} // else user not have permission, create chatreply (failed) and send to user sent
	} // else room not found
}

func (h *Hub) getStateHandle(m *pb.StateRequest, username string, clientIdx int) {
	// get old messages, get user in room ... and send to user
	room_id := m.GetRoomId()
	if h.chatRooms[room_id] == nil {
		return
	}
	room_name := h.chatRooms[room_id].room_name

	room_id_int, _ := strconv.Atoi(room_id)
	old_messages, err := db.GetMessagesInRoom(room_id_int, username, context.Background())
	var all_messages []*pb.UserMessage
	for _, msg := range old_messages {
		s, _ := msg.QuerySender().Only(context.Background())
		r, _ := msg.QueryRoom().Only(context.Background())
		r_str := strconv.Itoa(r.ID)
		usr_message := &pb.UserMessage{
			UserSent: s.Username,
			Message:  msg.Message,
			RoomId:   r_str,
		}
		all_messages = append(all_messages, usr_message)
	}
	if err != nil {
		log.Println("Cannot get old messages")
		return
	}
	//log.Println(room_id, room_name)
	var room_users []string
	for i := range h.chatRooms[room_id].users {
		room_users = append(room_users, i)
	}
	room_pass, err := db.GetRoomPassForOwner(room_id_int, username, context.Background())
	if err != nil {
		log.Println(err)
	}

	client := h.clients[username]
	if client != nil {

		if room_pass != "" {
			for _, clienti := range client {
				if clienti.idx == clientIdx {
					clienti.send <- &pb.WSMessage{
						Payload: &pb.WSMessage_StateReply{
							StateReply: &pb.StateReply{
								AllMessage:      all_messages,
								UserNamesInRoom: room_users,
								RoomName:        room_name,
								RoomPass:        room_pass,
							},
						},
					}
				}
			}
		} else {
			for _, clienti := range client {
				if clienti.idx == clientIdx {
					clienti.send <- &pb.WSMessage{
						Payload: &pb.WSMessage_StateReply{
							StateReply: &pb.StateReply{
								AllMessage:      all_messages,
								UserNamesInRoom: room_users,
								RoomName:        room_name,
							},
						},
					}
				}
			}
		}
	}
}

func (h *Hub) leaveRequestHandle(m *pb.LeaveRequest, username string, clientIdx int) {
	// check constraint, remove user from room
	room_id, _ := strconv.Atoi(m.GetRoomId())
	err := db.RemoveUserFromRoom(room_id, username, context.Background())
	h.update_room(room_id)
	if err != nil {
		log.Println("Cannot out room: %w", err)
		return
	}
	client := h.clients[username]
	if client != nil {
		for _, clienti := range client {
			clienti.send <- &pb.WSMessage{
				Payload: &pb.WSMessage_LeaveReply{
					LeaveReply: &pb.LeaveReply{
						Success: true,
					},
				},
			}
		}

	}
}

func (h *Hub) removeChannelRequestHandle(m *pb.RemoveChannelRequest, username string, clientIdx int) {
	room_id, _ := strconv.Atoi(m.GetRoomId())
	err := db.RemoveRoom(room_id, username, context.Background())
	if err != nil {
		log.Println(err)
		return
	}
	delete(h.chatRooms, m.GetRoomId())
	client := h.clients[username]
	if client != nil {
		for _, clienti := range client {
			clienti.send <- &pb.WSMessage{
				Payload: &pb.WSMessage_RemoveChannelReply{
					RemoveChannelReply: &pb.RemoveChannelReply{
						Success: true,
					},
				},
			}
		}
	}
}

func (h *Hub) joinRequestHandle(m *pb.JoinRequest, username string, clientIdx int) {
	room_pass := m.GetRoomPass()
	room_id, err := db.AddUserToRoomWithPass(room_pass, username, context.Background())
	if err != nil {
		log.Println("Join request failed: %w", err)
	}

	h.update_room(room_id)
	client := h.clients[username]
	if client != nil {
		for _, clienti := range client {
			clienti.send <- &pb.WSMessage{
				Payload: &pb.WSMessage_JoinReply{
					JoinReply: &pb.JoinReply{
						Success: true,
					},
				},
			}
		}
	}
}
