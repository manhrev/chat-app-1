package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	pb "gitlab.com/manhrev/chat-app1/api/chatpb"
	"gitlab.com/manhrev/chat-app1/auth"
	db "gitlab.com/manhrev/chat-app1/database"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type server struct {
	pb.UnimplementedUserSeviceServer
}

func main() {

	var err error
	//_, err := net.Listen("tcp", "127.0.0.1:8081")

	err = db.Init()
	if err != nil {
		log.Fatalf("error while connect database %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterUserSeviceServer(s, &server{})

	grpcWebServer := grpcweb.WrapServer(
		s,
		grpcweb.WithOriginFunc(func(origin string) bool { return true }),
	)

	srv := &http.Server{
		Handler: grpcWebServer,
		Addr:    "127.0.0.1:8082",
	}
	log.Println("Grpc web server listening at: ", srv.Addr)
	err = srv.ListenAndServe()

	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}

//APIs implement

//login
func (*server) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginReply, error) {

	err := db.UserLogin(req.Username, req.Password, ctx)
	if err != nil {
		log.Println("error logging in:", err)
		return nil, err
	}
	token, _ := auth.GenToken(req.Username)

	return &pb.LoginReply{
		Success: true,
		Token:   token,
	}, nil
}

func (*server) Signup(ctx context.Context, req *pb.SignupRequest) (*pb.SignupReply, error) {
	_, err := db.CreateUser(req.GetName(), req.GetUsername(), req.GetPassword(), ctx)
	if err != nil {
		log.Println("error creating user", err)
		return nil, fmt.Errorf("Cannot create account ")
	}

	log.Println("user created successfully")
	return &pb.SignupReply{
		Success: true,
	}, nil
}

func (*server) ChangeProfile(ctx context.Context, req *pb.ChangeProfileRequest) (*pb.ChangeProfileReply, error) {
	ms, ok := metadata.FromIncomingContext(ctx)
	var token string
	if ok {
		token = ms["token"][0]
	}
	//log.Println(token)
	username, err := auth.VerifyToken(token)
	if err != nil {
		return nil, fmt.Errorf("not logged in")
	}
	log.Println(username)
	return &pb.ChangeProfileReply{}, nil
}
