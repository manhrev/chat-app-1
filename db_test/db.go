package main

import (
	"context"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/manhrev/chat-app1/ent"
	"gitlab.com/manhrev/chat-app1/ent/room"
	"gitlab.com/manhrev/chat-app1/ent/user"
)

var client *ent.Client

func main() {
	var err error
	client, err = ent.Open("mysql", "root:manhre123654@tcp(localhost:3306)/chat?parseTime=True")
	if err != nil {
		log.Fatalf("failed opening connection to sql: %v", err)
	}

	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	//CreateUser("manh5", "manhre5", "123456", context.Background(), client)
	//Do()
	Room()
}

func Room() error {
	room, err := client.Room.Query().Where(room.ID(2)).Only(context.Background())
	if err != nil {
		log.Println(err)
	}
	log.Println(room.RoomName)

	return nil
}

func Do() error {
	user, err := client.User.Create().
		SetUsername("sample").
		SetName("hellokitty").
		SetPassword("123456").
		Save(context.Background())

	if err != nil {
		return err
	}
	log.Println("user:", user)
	rooms, err := user.QueryRoomsCreated().Count(context.Background())
	log.Println(rooms)
	room, err := client.Room.Create().
		SetOwner(user).
		SetRoomName("sample_room").
		AddUsers(user).
		Save(context.Background())
	log.Println("room:", room.QueryOwner())

	// mess, err := client.Message.Create().
	// 	SetMessage("sample message").
	// 	SetRoom(room).
	// 	SetSender(user).
	// 	Save(context.Background())
	// log.Println("mess:", mess)
	return nil
}

func Get() error {
	user, err := client.User.Query().Where(user.Username("sample")).Only(context.Background())

	if err != nil {
		log.Println(err)
		return err
	}
	rooms, err := user.QueryRoomsCreated().Count(context.Background())
	log.Println(rooms)
	return nil
}
