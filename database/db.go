package db

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/manhrev/chat-app1/ent"
	"gitlab.com/manhrev/chat-app1/ent/room"
	"gitlab.com/manhrev/chat-app1/ent/user"
)

var client *ent.Client

func Init() error {
	var err error
	client, err = ent.Open("mysql", "root:manhre123654@tcp(localhost:3306)/chat?parseTime=True")
	if err != nil {
		log.Fatalf("failed opening connection to sql: %v", err)
	}

	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	return nil
}

// for GRPC
//func Login()

// for WS
func CreateUser(name string, username string, password string, ctx context.Context) (*ent.User, error) {
	u, err := client.User.
		Create().
		SetUsername(username).
		SetName(name).
		SetPassword(password).
		Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to create user: %w", err)
	}
	return u, nil
}

func QueryUser(username string, ctx context.Context) (*ent.User, error) {
	u, err := client.User.
		Query().
		Where(user.Username(username)).
		Only(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to query user: %w", err)
	}
	return u, nil
}

func CreateRoom(owner string, room_name string, users []string, ctx context.Context) (*ent.Room, error) {
	u, err := QueryUser(owner, ctx)
	if err != nil {
		log.Println("Error while creating room: owner not found")
		return nil, err
	}
	//room string ! need better generator
	room_pass := randomString(6)
	room := client.Room.Create().
		SetOwner(u).
		SetRoomName(room_name).
		SetRoomPass(room_pass)

	for _, user := range users {
		u, err := QueryUser(user, ctx)
		if err != nil {
			log.Println("Error while creating room: a username is not found!")
			return nil, err
		}
		room = room.AddUsers(u)
	}
	r, err := room.Save(ctx)
	if err != nil {
		log.Println("Error while creating room: cannot save room: %w", err)
	}
	log.Println(r.RoomPass)
	//log.Print(r.QueryUsers().CountX(ctx))
	return r, nil
}

func GetRoomsForUser(username string, ctx context.Context) ([]*ent.Room, error) {
	user, err := client.User.
		Query().
		Where(user.Username(username)).
		Only(ctx)
	if err != nil {
		log.Println("Error while get room for user: cannot find user: %w", err)
		return nil, err
	}
	rooms, err := user.QueryRoomsJoined().All(ctx)
	if err != nil {
		log.Println("Error while getting rooms for user: %w", err)
		return nil, err
	}
	return rooms, nil
}

func GetRoomForUpdate(room_id int, ctx context.Context) (*ent.Room, []*ent.User, error) {
	roomc, err := client.Room.
		Query().
		Where(room.ID(room_id)).Only(ctx)
	if err != nil {
		return nil, nil, err
	}
	users, err := roomc.QueryUsers().All(ctx)
	if err != nil {
		return nil, nil, err
	}
	return roomc, users, nil
}

func SaveMessage(sender string, room_id int, message string, ctx context.Context) (*ent.Message, error) {
	user_sent, err := client.User.
		Query().
		Where(user.Username(sender)).
		Only(ctx)
	if err != nil {
		log.Println("Error while create message: cannot find sender: %w", err)
		return nil, err
	}

	room, err := client.Room.
		Query().
		Where(room.ID(room_id)).
		Only(ctx)
	if err != nil {
		log.Println("Error while create message: cannot find room: %w", err)
		return nil, err
	}
	db_message, err := client.Message.
		Create().
		SetMessage(message).
		SetSender(user_sent).
		SetRoom(room).Save(ctx)
	if err != nil {
		log.Println("Error while create message: cannot save message: %w", err)
		return nil, err
	}

	return db_message, nil
}

func GetMessagesInRoom(room_id int, username string, ctx context.Context) ([]*ent.Message, error) {

	room, err := client.Room.
		Query().
		Where(room.ID(room_id)).
		Only(ctx)
	if err != nil {
		log.Println("Error while get messages in room: cannot find room: %w", err)
		return nil, err
	}
	messages, err := room.QueryMessages().All(ctx)
	if err != nil {
		log.Println("Error while getting messages: %w", err)
		return nil, err
	}
	return messages, nil
}

func RemoveRoom(room_id int, username string, ctx context.Context) error {
	rom, err := client.Room.Query().Where(room.ID(room_id)).Only(ctx)
	if err != nil {
		return err
	}
	owner, err := rom.QueryOwner().Only(ctx)
	if err != nil {
		return err
	}
	if owner.Username != username {
		return fmt.Errorf("No permission to remove room")
	}
	err = client.Room.DeleteOneID(room_id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func RemoveUserFromRoom(room_id int, username string, ctx context.Context) error {
	room, err := client.Room.
		Query().
		Where(room.ID(room_id)).
		Only(ctx)
	if err != nil {
		log.Println("Error while removing user from room: cannot find room: %w", err)
		return err
	}
	owner, err := room.QueryOwner().Only(ctx)
	if err != nil {
		return err
	}
	users, err := room.QueryUsers().All(ctx)
	if err != nil {
		return err
	}
	//user want to leave is owner
	if owner.Username == username {
		room.Update().SetOwner(users[0])
	}

	//room only have one user left
	if len(users) == 1 {
		err := client.Room.DeleteOne(room).Exec(ctx)
		if err != nil {
			return err
		}
		return nil
	} else {

		var user_out *ent.User = nil
		for _, user := range users {
			log.Println(user.Username, username)
			if user.Username == username {
				user_out = user
			}
		}
		if user_out == nil {
			return fmt.Errorf("User not in room")
		}

		_, err := room.Update().RemoveUsers(user_out).Save(ctx)

		if err != nil {
			return err
		}
		return nil
	}

}

func AddUserToRoomWithPass(room_pass string, username string, ctx context.Context) (int, error) {
	usr, err := client.User.
		Query().
		Where(user.Username(username)).
		Only(ctx)
	if err != nil {
		return 0, err
	}
	roomp, err := client.Room.
		Query().
		Where(room.RoomPass(room_pass)).
		Only(ctx)
	if err != nil {
		return 0, fmt.Errorf("Error room not found: %v", err)
	}
	err = roomp.Update().AddUsers(usr).Exec(ctx)
	if err != nil {
		return 0, fmt.Errorf("Error add user to room: %v", err)
	}
	log.Println("ok")
	return roomp.ID, nil
}

func GetRoomPassForOwner(room_id int, owner string, ctx context.Context) (string, error) {
	roomp, err := client.Room.
		Query().
		Where(room.ID(room_id)).
		Only(ctx)
	if err != nil {
		return "", err
	}
	ownerp, err := roomp.QueryOwner().Only(ctx)
	if err != nil {
		return "", err
	}
	if ownerp.Username != owner {
		return "", fmt.Errorf("This user not owner, cannot show room pass")
	}

	return roomp.RoomPass, nil
}

func UserLogin(username string, password string, ctx context.Context) error {
	userp, err := client.User.
		Query().
		Where(user.Username(username)).
		Only(ctx)
	if err != nil {
		return fmt.Errorf("User not found")
	}
	if userp.Password != password {
		return fmt.Errorf("Wrong password")
	}
	return nil
}

//util
func randomString(length int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, length)
	rand.Read(b)
	return fmt.Sprintf("%x", b)[:length]
}
