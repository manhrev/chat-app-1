package auth

import (
	"fmt"
	"log"

	"github.com/golang-jwt/jwt"
)

var jwtKey = []byte("manh_secret_key")

func GenToken(username string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
	})
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		log.Println("Error while creating token: %w", err)
		return "", err
	}
	//log.Println(tokenString)
	return tokenString, nil
}

func VerifyToken(tokenString string) (string, error) {
	tok, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return jwtKey, nil
	})

	if err != nil {
		return "", fmt.Errorf("Cannot parse token: %v", err)
	}

	if claims, ok := tok.Claims.(jwt.MapClaims); ok && tok.Valid {
		//log.Println(claims["username"])
		v := claims["username"]
		switch c := v.(type) {
		case string:
			return c, nil
		}
		return "", fmt.Errorf("Error claim")
	} else {
		return "", fmt.Errorf("Error token")
	}
}
