package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("name"),
		field.String("username").Immutable().Unique(),
		field.String("password"),
		field.Time("created_at").Default(time.Now()).Immutable(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("rooms_created", Room.Type),
		edge.From("rooms_joined", Room.Type).
			Ref("users"),
		edge.To("messages_created", Message.Type),
	}
}
