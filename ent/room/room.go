// Code generated by entc, DO NOT EDIT.

package room

const (
	// Label holds the string label denoting the room type in the database.
	Label = "room"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldRoomName holds the string denoting the room_name field in the database.
	FieldRoomName = "room_name"
	// FieldRoomPass holds the string denoting the room_pass field in the database.
	FieldRoomPass = "room_pass"
	// EdgeOwner holds the string denoting the owner edge name in mutations.
	EdgeOwner = "owner"
	// EdgeUsers holds the string denoting the users edge name in mutations.
	EdgeUsers = "users"
	// EdgeMessages holds the string denoting the messages edge name in mutations.
	EdgeMessages = "messages"
	// Table holds the table name of the room in the database.
	Table = "rooms"
	// OwnerTable is the table that holds the owner relation/edge.
	OwnerTable = "rooms"
	// OwnerInverseTable is the table name for the User entity.
	// It exists in this package in order to avoid circular dependency with the "user" package.
	OwnerInverseTable = "users"
	// OwnerColumn is the table column denoting the owner relation/edge.
	OwnerColumn = "user_rooms_created"
	// UsersTable is the table that holds the users relation/edge. The primary key declared below.
	UsersTable = "room_users"
	// UsersInverseTable is the table name for the User entity.
	// It exists in this package in order to avoid circular dependency with the "user" package.
	UsersInverseTable = "users"
	// MessagesTable is the table that holds the messages relation/edge.
	MessagesTable = "messages"
	// MessagesInverseTable is the table name for the Message entity.
	// It exists in this package in order to avoid circular dependency with the "message" package.
	MessagesInverseTable = "messages"
	// MessagesColumn is the table column denoting the messages relation/edge.
	MessagesColumn = "room_messages"
)

// Columns holds all SQL columns for room fields.
var Columns = []string{
	FieldID,
	FieldRoomName,
	FieldRoomPass,
}

// ForeignKeys holds the SQL foreign-keys that are owned by the "rooms"
// table and are not defined as standalone fields in the schema.
var ForeignKeys = []string{
	"user_rooms_created",
}

var (
	// UsersPrimaryKey and UsersColumn2 are the table columns denoting the
	// primary key for the users relation (M2M).
	UsersPrimaryKey = []string{"room_id", "user_id"}
)

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	for i := range ForeignKeys {
		if column == ForeignKeys[i] {
			return true
		}
	}
	return false
}
